% MONADORO(1)
% Patryk Kocielnik
% September 2019

# NAME

monadoro -- count a 25-minute Pomodoro

# SYNOPSIS

**monadoro** [**-h**] [**`--session`**]

# DESCRIPTION

**monadoro** is a simple tool which serves as a Pomodoro timer. It has
two modes of operation: single-pomodoro (default), and a 4-pomodoro
session. It helps you concentrate on your goal while not losing track of
your surroundings.

# GENERAL OPTIONS

**-h**
:   Display a friendly help message.

**`--session`**
:   Start a 4-pomodoro session.

