#!/usr/bin/env stack
--stack --install-ghc runghc

module CountdownLoop       (countdown, countdownLoop) where

import System.Environment  (getArgs)
import Control.Concurrent  (threadDelay)
import Control.Monad       (void)
import ParseTime           (count_down_time)
import Display             (display)

milisecPerSecond :: Int
milisecPerSecond = 10 ^ (6 :: Int)

{-| wait: Pauses for the given number of seconds.

>>> wait 0
-}

wait :: Int -> IO()
wait n = threadDelay (n * milisecPerSecond)

withDelay :: IO()
withDelay = wait 1

-- Mocks a delay for test purposes.
noDelay :: IO()
noDelay = wait 0

{-| countdownLoop: Runs a Pomodoro timer.

>>> result <- countdownLoop noDelay "00:00"
...
>>> result
"00:00"

>>> result' <- countdownLoop noDelay "00:59"
...
>>> result'
"00:00"

>>> result'' <- countdownLoop noDelay "01:59"
...
>>> result''
"00:00"

-}

countdown :: String -> IO String
countdown = countdownLoop withDelay

countdownLoop :: IO() -> String -> IO String
countdownLoop delayer "00:00" = display "00:00" >> return "00:00"
countdownLoop delayer s = do
  display s
  delayer
  (countdownLoop delayer . count_down_time) s

parse :: [String] -> IO ()
parse [s] = void (countdownLoop withDelay s)
parse _   = void (countdownLoop withDelay "00:00")

main :: IO ()
main = getArgs >>= parse
