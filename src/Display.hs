module Display (display) where

import System.IO (hFlush, stdout)

display :: String -> IO ()
display s = putStr ("\r" ++ s) >> hFlush stdout
