#!/usr/bin/env stack
> --stack --install-ghc runghc

This is a command-line Pomodoro counter.

> module Pomodoro (session) where

> import Control.Concurrent (threadDelay)
> import Data.Time.Clock.POSIX (posixSecondsToUTCTime)
> import Data.Time.Format (formatTime, defaultTimeLocale)
> import Display (display)
>
> data Pomodoro = First | Second | Third | Fourth
>     deriving (Eq, Ord, Show, Read, Bounded, Enum)

> {-| Given an integer number of seconds, secToTimestamp
>  returns the corresponding time in MM:SS.
>
> >>> secToTimestamp 0
> "00:00"
> >>> secToTimestamp 1500
> "25:00"
> >>> secToTimestamp 3000
> "50:00"
> -}

> secToTimestamp :: Int -> String
> secToTimestamp = formatTime defaultTimeLocale "%M:%S"
>     . posixSecondsToUTCTime
>     . fromIntegral

For testing purposes, this function returns an empty action without delay.

Its type allows for drop-in replacement of the standard delay function.

> no_del :: Int -> IO()
> no_del _ = do
>     return ()

> pom :: Show a => IO () -> a -> IO ()
> pom delay m = do
>     let prefix = show m ++ " pomodoro" ++ " | "
>     wait_seconds delay prefix $ 25 * 60 + 1
>     putStrLn "Finished, take a 5 minute rest."

> rest :: IO () -> Int -> IO ()
> rest delay minutes = do
>     let prefix = "Rest time" ++ " | "
>     wait_seconds delay prefix $ minutes * 60 + 1

> pomodoro :: IO () -> Pomodoro -> IO ()
> pomodoro delay Fourth = do
>     pom delay Fourth
>     putStrLn "Take a 30-minute rest now. You just completed 4 pomodoros."
>     putStrLn "Congratulations on 4 pomodoros finished in a row!"
> pomodoro delay m = do
>     pom delay m
>     rest delay 5
>     putStrLn "Get back to work." >> delay
>     pomodoro delay $ succ m
>
> wait_seconds :: IO () -> String -> Int -> IO()
> wait_seconds delay prefix 0 = display ""
> wait_seconds delay prefix n = do
>     display $ prefix ++ (secToTimestamp (n - 1))
>     delay
>     wait_seconds delay prefix $ n - 1

> session :: IO() -> IO()
> session delay = pomodoro delay First
