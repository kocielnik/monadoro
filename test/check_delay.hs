#!/usr/bin/env runghc

import System.Process (callCommand, readProcessWithExitCode)
import System.Exit    (die)

filterOutNewline :: String -> String
filterOutNewline = filter (not . (`elem` "\n"))

getUnixTime :: IO Int
getUnixTime = do
    let stdin' = ""
    (_, stdout', _) <- readProcessWithExitCode "date" ["+%s"] stdin'
    let timeString = filterOutNewline stdout'
    let time = read timeString :: Int
    return time

getDelay :: IO Int
getDelay = do
    start <- getUnixTime
    callCommand "sleep 1"
    end <- getUnixTime
    return (end - start)

testDelay :: IO Bool
testDelay = do
    delay <- getDelay
    let result = delay == 1
    return result

main :: IO ()
main = do
    result <- testDelay
    if not result then
        die "Delay test failed"
    else
        print "Delay test OK"
