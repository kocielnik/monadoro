import GHC.Stack.Types   (HasCallStack)
import Test.DocTest      (doctest)

assert :: HasCallStack => Bool -> String
assert False = error "Assertion failed!"
assert _ = ""

checkCondition :: HasCallStack => IO ()
checkCondition
  | result == "" = putStr ""
  | otherwise = putStrLn result
    where result = assert (0 /= 1)

runOtherTests :: HasCallStack => IO ()
runOtherTests = checkCondition

runDocTests :: IO ()
runDocTests = doctest [
        "-isrc",
        "src/CLI.hs",
        "src/ParseTime.lhs",
        "src/CountdownLoop.hs",
        "src/Pomodoro.lhs"
    ]

main :: IO ()
main = runDocTests >> runOtherTests

-- These tests take 5 seconds to execute even when
-- *nothing* was changed!
--
-- If something *was* changed, they take between *18
-- and 26* seconds.
--
-- Pytest wouldn't take a *single second* to test all
-- this! Can we get closer to that result?
