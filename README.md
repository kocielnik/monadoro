# Monadoro

A simple pomodoro counter. Type `monadoro` to get started.

![A completed session of 4 pomodoros.](doc/session_completed.png)

# The goal

The simplest Pomodoro counter, ready to deploy at once on most command-line
platforms to enable you to focus on your work in just a few moments, for weeks
to come.

# The method

There are six steps in the original technique [^cirillo_pomodoro]:

1. Choose a task you'd like to get done,
2. Set the Pomodoro for 25 minutes,
3. Work on the task until the Pomodoro rings,
4. When the Pomodoro rings, put a checkmark on a paper,
5. If you have fewer than four checkmarks, take a short break (3-5 minutes),
   then go to step 2;
6. After four pomodoros, take a longer break (15-30 minutes), reset your
   checkmark count to zero, then go to step 1.

# Installing

## Fastest: With Nix already installed

`nix-shell -p haskellPackages.Monadoro --run monadoro`

## Using Stack

1. Ensure you have `stack` installed.
    - Vendor-recommended way: `curl -sSL https://get.haskellstack.org/ | sh`
    - Native install, on Ubuntu as an example: `apt install haskell-stack`
2. Install Monadoro
    1. Update the Stack index: `stack update`
    2. Install: `stack install Monadoro`. Note the capital "M" in the package
       name.

## Test suite dependencies

DocTest 0.16.1 Haskell library requires libtinfo to compile (`libtinfo-dev` in
Apt on Ubuntu).

# Credits

Initial work: GitHub user Elektroingenieur [^elektroingenieur], author of The
Haskell Blog [^haskell_blog].

Manpage generation from Markdown: Jérôme Belleman [^belleman] and Pandoc
[^pandoc] project.

"Pomodoro Technique (illustration)" (CC BY 2.0) by [Michael Zero
Mayer](https://www.flickr.com/photos/michael_mayer/6969282632/).

# References

[^cirillo_pomodoro]: [The Pomodoro Technique](https://francescocirillo.com/pages/pomodoro-technique), Francesco Cirillo
[^haskell_blog]: https://haskellblog.wordpress.com/2015/08/05/pomodoro-timer-in-haskell/
[^elektroingenieur]: https://github.com/Elektroingenieur
[^belleman]: http://jeromebelleman.gitlab.io/posts/publishing/manpages/
[^pandoc]: pandoc.org
