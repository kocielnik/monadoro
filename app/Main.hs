module Main where

import System.Environment (getArgs)
import CLI                (containsEither,
                           filterParams,
                           printVersion,
                           runPomodoros,
                           runSession,
                           showManual,
                           showUsage)


main :: IO ()
main = getArgs >>= parse

parse :: [String] -> IO ()
parse args
  | args `containsEither` ["--help", "-h"]    = showUsage
  | args `containsEither` ["--version", "-v"] = printVersion
  | args `containsEither` ["--session", "-s"] = runSession args
  | args `containsEither` ["--man", "-m"]     = showManual
  | otherwise                                 = runPomodoros args
