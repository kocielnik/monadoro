bin=monadoro
src_dir=Monadoro

version=$(shell grep '^[Vv]ersion:' package.yaml | awk '{print $$2;}')

run:
	@cd $(src_dir) && stack build && stack exec monadoro
test:
	@cd $(src_dir)/src && ./Pomodoro.lhs
install:
	@cd $(src_dir) && stack install
remove:
	rm -f `which $(bin)`
	#rm ~/.local/bin/monadoro
ed:
	vim $(src_dir)/src/Pomodoro.lhs
version:
	@echo $(version)
manpage:
	pandoc $(src_dir)/man/monadoro.md -s -t man | /usr/bin/man -l -
man/monadoro.1:
	pandoc $(src_dir)/man/monadoro.md -s -t man -o $(src_dir)/man/monadoro.1
install_man:
	cp $(src_dir)/man/monadoro.1 $(HOME)/.local/share/man/man1/
